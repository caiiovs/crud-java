<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Alterar</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            
    </head>
    <body>
        <form action="executa_alterar.jsp" method="post">
            <h3>Editar produto</h3>
            <div class="row">
                <div class="col s12">
                  <div class="row">
                     <div class="input-field col s6">
                      <input placeholder="Codigo" id="codigo"  name="codigo" value="<%=request.getParameter("codigo")%>" type="text" class="validate">
                      <label for="codigo">Codigo</label>
                    </div>
                    <div class="input-field col s6">
                      <input placeholder="Descri��o" id="descricao" name="descricao" value="<%=request.getParameter("descricao")%>" type="text" class="validate">
                      <label for="descricao">Descri��o</label>
                    </div>
                    <div class="input-field col s6">
                      <input placeholder="Pre�o" id="Pre�o" type="text" name="preco" class="validate" value="<%=request.getParameter("preco")%>">
                      <label for="Pre�o">Pre�o</label>
                    </div>
                  </div>
                  <button class="waves-effect waves-light btn" type="submit">Salvar</button>
                </div>
            </div>
        </form>
</html>
