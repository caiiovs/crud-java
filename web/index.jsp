<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="dao.ProdutoDAO"%>
<%@page import="model.Produto"%>
<%@page import="java.util.ArrayList"%>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Inserir</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
            
    </head>
    <body>
        <form action="index.jsp" method="post">
            <div class="row">
                <div class="col s12">
                  <div class="row">
                    <div class="input-field col s6">
                      <input placeholder="Descrição" id="descricao" name="descricao" type="text" class="validate">
                      <label for="descricao">Descrição</label>
                    </div>
                    <button class="waves-effect waves-light btn" type="submit">ok</button>
                  </div>
                </div>
            </div>
        </form>
        <%
            try{
                out.print("<table>");
                out.print("<tr>");
                out.print("<th>Codigo</th><th>Descrição</th><th>Preço</th><th>Editar</th><th>Excluir</th>");
                ProdutoDAO prd = new ProdutoDAO();
                if(request.getParameter("descricao") == null){
                    ArrayList<Produto> lista = prd.listarTodos();
                    for(int num = 0; num < lista.size(); num++){
                         out.print("<tr>");
                         out.print("<td>"+lista.get(num).getCodigo()+"</td>");
                         out.print("<td>"+lista.get(num).getDescricao()+"</td>");
                         out.print("<td>"+lista.get(num).getPreco()+"</td>");
                         out.print("<td><a href='alterar.jsp?codigo="+lista.get(num).getCodigo()+"&descricao="+lista.get(num).getDescricao()+"&preco="+lista.get(num).getPreco()+"'>CLIQUE</a></td>");
                         out.print("<td><a href='excluir.jsp?codigo="+lista.get(num).getCodigo()+"&descricao="+lista.get(num).getDescricao()+"'>CLIQUE</a></td>");
                         out.print("</tr>");

                    }
                }else{
                    ArrayList<Produto> lista = prd.listarTodosDescricao(request.getParameter("descricao"));
                    for(int num = 0; num < lista.size(); num++){
                         out.print("<tr>");
                         out.print("<td>"+lista.get(num).getCodigo()+"</td>");
                         out.print("<td>"+lista.get(num).getDescricao()+"</td>");
                         out.print("<td>"+lista.get(num).getPreco()+"</td>");
                         out.print("<td><a href='alterar.jsp?codigo="+lista.get(num).getCodigo()+"&descricao="+lista.get(num).getDescricao()+"&preco="+lista.get(num).getPreco()+"'>CLIQUE</a></td>");
                         out.print("<td><a href='excluir.jsp?codigo="+lista.get(num).getCodigo()+"&descricao="+lista.get(num).getDescricao()+"'>CLIQUE</a></td>");
                         out.print("</tr>");

                    }
                }
                out.print("</tr>");
                out.print("</table>");
            }catch(Exception erro){
                throw new RuntimeException("Erro 10 :" + erro);
            }
        %>
        <a href="inserir.jsp">NOVO</a>
</html>
